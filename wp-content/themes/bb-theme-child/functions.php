<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );


add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);   
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);    
});

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.@$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );



function my_bb_custom_fonts ( $system_fonts ) {

  $system_fonts[ 'Vela Sans ExtLt' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '200',
    ),
  );

  $system_fonts[ 'Vela Sans' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '400',
    ),
  );
  
  $system_fonts[ 'Vela Sans Med' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '500',
    ),
  );	
 	
  $system_fonts[ 'Vela Sans Bd' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '700',
    ),
  );	
	
   $system_fonts[ 'Vela Sans ExtBd' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '900',
    ),
  );	



    return $system_fonts;
}

//Add to Beaver Builder Theme Customizer
add_filter( 'fl_theme_system_fonts', 'my_bb_custom_fonts' );

//Add to Beaver Builder modules
add_filter( 'fl_builder_font_families_system', 'my_bb_custom_fonts' );


add_filter( 'gform_pre_render_17', 'prepopulate_procategory' );
add_filter( 'gform_pre_validation_17', 'prepopulate_procategory' );
add_filter( 'gform_pre_submission_filter_17', 'prepopulate_procategory' );
add_filter( 'gform_admin_pre_render_17', 'prepopulate_procategory' );
function prepopulate_procategory( $form ) {

    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'f2cproductcategory' ) === false ) {
            continue;
        }
 
        $website_json =  json_decode(get_option('website_json'));
        $website_json = (array)$website_json;
        $website_option = (array)$website_json['options'];      

        $procatarray = array_search('0', $website_option, true);       
        unset($website_option['allowBlogs']);  

        foreach($website_option as $key => $value ){
        
          if ($value == false) {
            unset($website_option[$key]);

           }

          }

        $catmapping = array(
            "Hardwood"=>"hardwoodLanding",
            "Kitchen Bath Vanity"=>"kitchenBathVanity",
            "Kitchen Bath Countertop"=>"kitchenBathCountertop",
            "Carpet"=>"carpetLanding",
            "Kitchen Bath Hardware"=>"kitchenBathHardware",
            "Window Treatment"=>"windowTreatment",
            "Vinyl"=>"vinylLanding",
            "Tile"=>"tileLanding",
            "Kitchen Bath"=>"kitchenBath",
            "Kitchen Bath Cabinet"=>"kitchenBathCabinet",
            "Laminate"=>"laminateLanding",
            "Area Rugs"=>"areaRugsLanding",
        );       

      //  ksort($website_option);

        $order = array('carpetLanding', 'hardwoodLanding', 'laminateLanding', 'vinylLanding', 'tileLanding','kitchenBath','kitchenBathHardware','kitchenBathCountertop','kitchenBathVanity','kitchenBathCabinet','windowTreatment');
        
       // $ordered_array = array_merge(array_flip($order), $website_option);

       // write_log($ordered_array);
 
        $choices = array(); 
        
        foreach($website_option as $x => $val) {

            $ch_cat = array_search($x,$catmapping);
            $choices[] = array( 'text' => $ch_cat, 'value' => $ch_cat );
        }        
        
        $field->placeholder = 'Select Product Category of Interest';
        $field->choices = $choices;
 
    }
 
    return $form;

}