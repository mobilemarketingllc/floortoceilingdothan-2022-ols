<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 
           $col_class = 'col-md-3 col-sm-4 col-xs-6';
    ?>

    
<?php while ( have_posts() ): the_post(); ?>
<?php if(get_field('swatch_image_link')) {
    
    //collection field
    $collection = get_field('collection', $post->ID);?>
    <div class="<?php echo $col_class; ?>">
    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
	
						$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');		
							
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="/wp-content/plugins/grand-child/product-listing-templates/images/exclusive-icon.png" alt="<?php the_title(); ?>" /></span>
			<?php } ?>      
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php the_field('collection'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
			
			 <?php 

             
     /*       
$promsale =  json_decode(get_option('saleconfiginformation')); 

//print_r($promsale)

$sale_arr = array();

$brand_arr = array();

$i = 0 ;



foreach ($promsale as $sale) {
   
    if($sale->getCoupon == 1){

       $brand_arr = array_merge($brand_arr,$sale->brandList);

       $sale_end_date   =  date("d-m-Y", substr($sale->endDate, 0, 10)); 
       $sale_start_date =  date("d-m-Y", substr($sale->startDate, 0, 10)); 

       $sale_arr[$i]['promoCode'] = $sale->promoCode;
       $sale_arr[$i]['name']      = $sale->name; 
       $sale_arr[$i]['startDate'] = $sale_start_date; 
       $sale_arr[$i]['endDate']   = $sale_end_date;   
       $sale_arr[$i]['getCoupon'] = $sale->getCoupon;   
       $sale_arr[$i]['brandList'] = $sale->brandList;   


       $i++;
    }
}*/

?>          
            <a class="link plp-view-product fl-button " href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
            <?php if($show_financing == 1){?>
            <a href="<?php echo site_url(); ?>/flooring-financing/" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text">GET FINANCING</span>
            </a><br />
           <?php } ?>
			 <?php if( get_option('getcouponbtn') == 1){  ?>
                <a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="link getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>
           
            
        </div>
    </div>
    </div>
        <?php } else    
        {
            $show_thumbs = FLTheme::get_setting( 'fl-archive-show-thumbs' );
            $show_full   = apply_filters( 'fl_archive_show_full', FLTheme::get_setting( 'fl-archive-show-full' ) );
            $more_text   = FLTheme::get_setting( 'fl-archive-readmore-text' );
            $thumb_size  = FLTheme::get_setting( 'fl-archive-thumb-size', 'large' );
            $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
            if(empty($url))
            {
                $feat_img = "/wp-content/themes/bb-theme-child/images/default.jpg";
            }
            else
            {
                $feat_img = $url;
            }
            ?>
<div class="col-md-3 col-sm-4 col-xs-6">
    
    <div class="fl-post-grid-post">
        <div class="fl-post-grid-image">
                <a href="<?php the_permalink();?>" title=""><img class="shadow" src="<?php echo $feat_img;?>" alt="<?php the_title_attribute();?>"></a>
        </div>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"><?php the_title(); ?></a>
            </h2>
			<a href="<?php the_permalink();?>" target="_self" class="fl-button getcoupon-btn" role="button">
                <span class="fl-button-text">READ MORE</span>
            </a>
            <br>                                    
        </div>
    </div>
</div>
        <?php }?>
<?php endwhile; ?>
</div>
</div>